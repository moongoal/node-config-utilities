# Changelog

Outstanding changes to the project.

## [v1.2.0] - 2019-04-26
### Changed
- Path resolution rules to support Node 12
- Docker image to Node 12
- Dependencies updated

## Fixed
- Source documentation not represented as JSDoc comments

## [v1.1.0] - 2019-01-20
### Added
- Code coverage
- CI support
- NPM ignore file

### Fixed
- Wrong test file structure didn't allow for execution of two tests
- README.md documentation for `resolvePaths({ normalize })` not showing default value

### Changed
- Removed test categories "Options" and "Arguments"

## [v1.0.4] - 2019-01-14
### Fixed
- Empty arrays not allowed in options

## [v1.0.3] - 2019-01-11
### Changed
- Refactored dependencies

## [v1.0.2] - 2019-01-11
### Fixed
- `readTasks()` wrapper tasks not passing the provided arguments to the original functions

### Added
- Test to ensure wrapper tasks are provided with updated displayName

## [v1.0.1] - 2019-01-07
### Fixed
- `readTasks()` now returns new wrapper-task-functions with proper displayName set

### Added
- Changelog file
