/* eslint-env mocha, chai */
/* eslint-disable no-unused-expressions */

const os = require('os');
const fs = require('fs').promises;
const path = require('path');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const del = require('del');
const { realpathSync } = require('fs');
const {
  DEFAULT_TASK_DIR,
  DEFAULT_CONFIG_DIR,
  resolvePaths,
  readConfig,
  readTasks
} = require('.');

const { expect } = chai;
const WIN32 = os.platform() === 'win32';

chai.use(sinonChai);

let oldCwd; let tempDirPath; let tempCfgPath; let
  tempTaskPath;
const modules = ['index', 'paths', 'xrowserify', 'mocha', 'eslint'];

before(function () {
  return fs.mkdtemp(
    path.join(os.tmpdir(), 'config-utilities-test-')
  )
    .then((folder) => { tempDirPath = realpathSync(folder); return tempDirPath; })
    .then((folder) => {
      oldCwd = process.cwd();
      tempCfgPath = path.join(folder, DEFAULT_CONFIG_DIR);
      tempTaskPath = path.join(folder, DEFAULT_TASK_DIR);

      return Promise.all([
        fs.mkdir(tempCfgPath),
        fs.mkdir(tempTaskPath)
      ]);
    })
    .then(() => Promise.all(
      [tempCfgPath, tempTaskPath].map(folder => (
        modules
          .map(modName => fs.writeFile(
            path.join(folder, `${modName}.js`),
            `
              function task_${modName}(cb) { cb(); };
              function namedTask_${modName}() {};

              namedTask_${modName}.displayName = 'named-task-${modName}';

              module.exports = {
                loaded: "yes",
                task_${modName},
                namedTask_${modName}
              }
            `
          ))
      ))
    ))
    .then(() => {
      process.chdir(tempDirPath);
    });
});

after(function () {
  process.chdir(oldCwd);
  del(tempDirPath, { force: true });
});

describe('resolvePaths()', function () {
  const allForwardSlashes = ({ a }) => !a.includes('\\');
  const paths = {
    myFolder1: '.',
    myFolder2: '/home',
    specialFile: '$myFolder2/special.txt',
    myFile: 'hey',
  };

  it('should return the expected values', function () {
    expect(resolvePaths(paths)).to.deep.equal({
      myFolder1: path.resolve(process.cwd()),
      myFolder2: path.resolve('/home'),
      specialFile: path.resolve('/home/special.txt'),
      myFile: path.resolve(process.cwd(), 'hey'),
    });
  });

  it('should respect the baseDir option', function () {
    const baseDir = tempCfgPath;

    expect(resolvePaths(paths, { baseDir })).to.deep.equal({
      myFolder1: path.resolve(baseDir),
      myFolder2: path.resolve('/home'),
      specialFile: path.resolve('/home/special.txt'),
      myFile: path.resolve(baseDir, 'hey'),
    });
  });

  it('should throw if an iteration limit is exceeded', function () {
    expect(() => resolvePaths({
      ...paths,
      specialFile2: '$myFolder/nothing',
    }, { iterationLimit: 1 })).to.throw();
  });

  if (WIN32) {
    it('should respect the normalize option', function () {
      expect(
        resolvePaths({ a: '/a/b' }),
      ).not.to.satisfy(allForwardSlashes);

      expect(
        resolvePaths({ a: '/a/b' }, { normalize: true }),
      ).to.satisfy(allForwardSlashes);
    });
  } else it('(Windows-specific test)');

  it('should respect the relative option', function () {
    expect(
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { baseDir: tempCfgPath, relative: false })
    ).to.have.property(
      'userHome', (
        path.join(tempCfgPath, 'user/second')
      )
    );

    expect(
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { baseDir: tempCfgPath, relative: true })
    ).to.have.property(
      'userHome', (
        WIN32
          ? 'user\\second'
          : 'user/second'
      )
    );
  });

  it('should only allow option "iterationLimit" to be an integer', function () {
    expect(function () {
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { iterationLimit: '/' });
    }).to.throw();

    expect(function () {
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { iterationLimit: 1000 });
    }).not.to.throw();
  });

  it('should only allow the "baseDir" option to be a string', function () {
    expect(function () {
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { baseDir: '/' });
    }).not.to.throw();

    expect(function () {
      resolvePaths({
        home: '.',
        userHome: '$home/user/second'
      }, { baseDir: 1000 });
    }).to.throw();
  });
});

describe('readConfig()', function () {
  it('should load all the configuration modules', function () {
    expect(readConfig())
      .to.be.an('object')
      .and.to.include.all.keys(modules);
  });

  it('should not load excluded modules', function () {
    expect(readConfig(tempCfgPath, { exclude: ['paths'] }))
      .to.not.have.any.keys(['paths']);
  });

  it('should tranform module names if a transform function is provided', function () {
    const tf1 = modName => (modName === 'paths' ? 'paths-changed' : modName);
    const tf2 = (modName, modContent) => (modContent.namedTask_paths ? 'paths-changed' : modName);

    expect(readConfig(tempCfgPath, { transform: tf1 }))
      .to.include.all.keys(['paths-changed'])
      .and.to.not.have.any.keys(['paths']);

    expect(readConfig(tempCfgPath, { transform: tf2 }))
      .to.include.all.keys(['paths-changed'])
      .and.to.not.have.any.keys(['paths']);
  });

  it('should only allow a string configuration folder argument', function () {
    expect(function () { readConfig(5); }).to.throw();
  });

  it('should only allow the "exclude" option to be of type string or Array[string]', function () {
    expect(function () { readConfig(tempDirPath, { exclude: { something: 1 } }); })
      .to.throw();

    expect(function () { readConfig(tempDirPath, { exclude: { something: [1] } }); })
      .to.throw();

    expect(function () { readConfig(tempDirPath, { exclude: 'a' }); })
      .not.to.throw();

    expect(function () { readConfig(tempDirPath, { exclude: ['a'] }); })
      .not.to.throw();
  });

  it('should only allow the "transform" option to be a function', function () {
    expect(function () { readConfig(tempDirPath, { transform: { something: 1 } }); })
      .to.throw();

    expect(function () { readConfig(tempDirPath, { transform: x => x }); })
      .not.to.throw();
  });

  it('should allow the function to be called with the "opts" argument only', function () {
    const opts = { exclude: ['paths'] };
    const r1 = readConfig(opts);
    const r2 = readConfig(tempCfgPath, opts);

    expect(r1).to.deep.equal(r2);

    expect(function () { readConfig({}); })
      .to.not.throw();
  });

  it('should allow the "exclude" option to be an empty array', function () {
    expect(function () { readConfig({ exclude: [] }); }).not.to.throw();
  });
});

describe('readTasks()', function () {
  it('should load all the task files', function () {
    expect(readTasks(tempTaskPath))
      .to.be.an('object')
      .and.to.include.all.keys(['task_index', 'named-task-index'])
      .and.to.not.include.any.keys(['namedTask_index']);
  });

  it('should pass the arguments from the wrapper to the actual task function', function () {
    const tasks = readTasks(tempTaskPath);
    const f = sinon.fake();
    const taskName = `task_${modules[0]}`;

    tasks[taskName](f);

    expect(f).to.have.been.called;
  });

  it('should update wrapper task functions displayName', function () {
    const tasks = readTasks(tempTaskPath, {
      transform: taskName => taskName.replace(/_/, '-')
    });
    const taskName = `task-${modules[0]}`;

    expect(tasks).to.include.all.keys([taskName]);
  });

  it('should not load excluded modules', function () {
    expect(readTasks(tempTaskPath, { exclude: ['paths'] }))
      .to.not.have.any.keys(['paths']);
  });

  it('should not return excluded tasks', function () {
    expect(readTasks(tempTaskPath, { excludeTasks: 'task_index' }))
      .not.to.have.any.key(['task_index']);
  });

  it('should transform the returned task names according to the "transform" option', function () {
    expect(readTasks(tempTaskPath, { transform: taskName => taskName.toUpperCase() }))
      .to.include.all.keys(['TASK_INDEX'])
      .but.not.to.have.any.keys(['task_index']);

    expect(readTasks(tempTaskPath))
      .to.include.all.keys(['task_index'])
      .but.not.to.include.any.keys(['TASK_INDEX']);
  });

  it('should allow the function to be called with the "opts" argument only', function () {
    const tasks = modules.slice().map(modName => `task_${modName}`);
    const removedTasks = tasks.splice(tasks.indexOf('task_paths'), 1);
    const opts = { excludeTasks: removedTasks };
    const r1 = readTasks(opts);
    const r2 = readTasks(tempTaskPath, opts);

    expect(r1)
      .to.include.all.keys(tasks)
      .but.not.to.include.any.keys(removedTasks);

    expect(r2)
      .to.include.all.keys(tasks)
      .but.not.to.include.any.keys(removedTasks);

    expect(function () { readTasks({}); })
      .to.not.throw();
  });

  it('should only allow a string task folder argument', function () {
    expect(function () { readTasks(5); }).to.throw();
  });

  it('should only allow the "exclude" option to be of type string or Array[string]', function () {
    expect(function () { readTasks(tempDirPath, { exclude: { something: 1 } }); })
      .to.throw();

    expect(function () { readTasks(tempDirPath, { exclude: { something: [1] } }); })
      .to.throw();

    expect(function () { readTasks(tempDirPath, { exclude: 'a' }); })
      .not.to.throw();

    expect(function () { readTasks(tempDirPath, { exclude: ['a'] }); })
      .not.to.throw();
  });

  it('should only allow the "excludeTasks" option to be of type string or Array[string]', function () {
    expect(function () { readTasks(tempDirPath, { excludeTasks: { something: 1 } }); })
      .to.throw();

    expect(function () { readTasks(tempDirPath, { excludeTasks: { something: [1] } }); })
      .to.throw();

    expect(function () { readTasks(tempDirPath, { excludeTasks: 'a' }); })
      .not.to.throw();

    expect(function () { readTasks(tempDirPath, { excludeTasks: ['a'] }); })
      .not.to.throw();
  });

  it('should only allow the "transform" option to be a function', function () {
    expect(function () { readTasks(tempDirPath, { transform: 'asd' }); })
      .to.throw();

    expect(function () { readTasks(tempDirPath, { transform: x => x }); })
      .not.to.throw();
  });

  it('should allow the "exclude" option to be an empty array', function () {
    expect(function () { readConfig({ exclude: [] }); }).not.to.throw();
  });

  it('should allow the "excludeTasks" option to be an empty array', function () {
    expect(function () { readConfig({ excludeTasks: [] }); }).not.to.throw();
  });
});
